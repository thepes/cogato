package org.copains.cogato.logging.manager;

import org.copains.cogato.logging.objects.LogEntry;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Calendar;

public class LogMg {
	
	public static LogEntry save(LogEntry log) {
		if (null == log.getCreationDate()) {
			log.setCreationDate(Calendar.getInstance().getTime());
		}
		ofy().save().entity(log).now();
		return log;
	}

}
