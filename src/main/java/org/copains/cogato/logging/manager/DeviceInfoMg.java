package org.copains.cogato.logging.manager;

import org.copains.cogato.logging.objects.MobileDeviceInfo;
import static com.googlecode.objectify.ObjectifyService.ofy;

public class DeviceInfoMg {
	
	public static MobileDeviceInfo save(MobileDeviceInfo info) {
		ofy().save().entity(info).now();
		return info;
	}
	
}
