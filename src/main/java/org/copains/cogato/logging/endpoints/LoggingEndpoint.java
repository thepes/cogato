package org.copains.cogato.logging.endpoints;

import org.copains.cogato.logging.manager.DeviceInfoMg;
import org.copains.cogato.logging.manager.LogMg;
import org.copains.cogato.logging.objects.MobileDeviceInfo;
import org.copains.cogato.logging.objects.LogEntry;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.jackson.ObjectifyJacksonModule;

@Api(
		name = "loggingApi",
        version = "v1",
        resource = "log",
        namespace = @ApiNamespace(
                ownerDomain = "cogato.copains.org",
                ownerName = "cogato.copains.org",
                packagePath = "")
		)
public class LoggingEndpoint {
	
	static {
		ObjectifyService.register(LogEntry.class);
		ObjectifyService.register(MobileDeviceInfo.class);
	}
	
	@ApiMethod(
			name = "create",
			path = "log",
			httpMethod = HttpMethod.POST
			)
	public LogEntry create(LogEntry entry) {
		//ObjectifyService.ofy()
		if (null != LogMg.save(entry)) {
			MobileDeviceInfo inf = entry.getDeviceInfo();
			if (null != inf) {
				inf.setId(entry.getId());
				DeviceInfoMg.save(inf);
			}
		}
		
		return entry;
	}

}
