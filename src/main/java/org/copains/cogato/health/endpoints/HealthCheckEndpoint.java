package org.copains.cogato.health.endpoints;

import org.copains.cogato.health.objects.HealthCheck;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.ApiNamespace;

@Api(
		name = "healthcheckApi",
        version = "v1",
        resource = "healthcheck",
        namespace = @ApiNamespace(
                ownerDomain = "cogato.copains.org",
                ownerName = "cogato.copains.org",
                packagePath = ""
  ))
public class HealthCheckEndpoint {

	@ApiMethod(
			path = "healthcheck",
			httpMethod = HttpMethod.GET,
			name = "healthcheck"
			)
	public HealthCheck check() {
		HealthCheck c = new HealthCheck();
		c.setStatus("OK");
		return c;
	}
	
}
